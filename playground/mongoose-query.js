const {ObjectId} = require('mongodb');

const {mongoose} = require('../server/db/mongoose');
const {Todo} = require('../server/models/todo');
const {User} = require('../server/models/user');

var id = '5aafb088b189a8267cd95780';

if(!ObjectId.isValid(id)) {
    console.log('User is not valid');
} 

// Todo.find({_id:id}).then((todos)=>{
//     console.log('todos',todos);
// });

// Todo.findOne({_id:id}).then((todo)=>{
//     console.log('todo',todo);
// });

// Todo.findById(id).then((todo)=>{
//     if(!todo) {
//         return console.log('Unable to find user');
//     }
//     console.log(todo);
// });

User.findById(id).then((user)=>{
    if(!user) {
        return console.log('unable to find user');
    }
    console.log(user);
}).catch((e)=>{console.log(e)});