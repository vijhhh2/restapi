const {MongoClient, ObjectID} = require('mongodb');



MongoClient.connect('mongodb://localhost:27017/TodoApi',(err,client)=> {

    if(err) {
      return console.log(`Error:${err}`);
    }
    console.log('connection succedd');

    const db = client.db('TodoApi');

    // deleteMany

    // db.collection('Todos').deleteMany({text: 'eat lunch'}).then((res)=>{
    //     console.log(res);
    // });
    // db.collection('Users').deleteMany({name: 'vijay'}).then((res)=>{
    //     console.log(res);
    // });

    // deleteOne

    // db.collection('Todos').deleteOne({text: 'eat lunch'}).then((res)=> {
    //     console.log(res);
    // });

    // findOneAndDelete

        // db.collection('Todos').findOneAndDelete({completed:false}).then((res)=>{
        //     console.log(res);
        // });
        db.collection('Users').findOneAndDelete({_id: new ObjectID('5aa930d664223e9847ba990b')}).then((res)=>{
            console.log(res);
        });

    // client.close();
});