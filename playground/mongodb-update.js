const {MongoClient, ObjectID} = require('mongodb');



MongoClient.connect('mongodb://localhost:27017/TodoApi',(err,client)=> {

    if(err) {
      return console.log(`Error:${err}`);
    }
    console.log('connection succedd');

    const db = client.db('TodoApi');

    // db.collection('Todos').findOneAndUpdate({_id: new ObjectID("5aa936a064223e9847ba9911")},
    // {
    //     $set:{
    //         completed: true
    //     }
    // },{returnOriginal : false}).then((res)=> {
    //     console.log(res);
    // });
    db.collection('Users').findOneAndUpdate({_id: new ObjectID('5aa930cd64223e9847ba990a')},{
        $set: {name: 'Vijay Kumar Reddy'},
        $inc: {age:1}
    },{returnOriginal:false}).then((res)=>{
        console.log(res);
    });

    // client.close();
});